//var currentLocation;
var placeArray = [];
var map;
var geocoder;
var listDisplay;
var currentPlaceSelection;
var sunriseValue;
var sunsetValue;
var currentLatLng;
var usableCoordsForPOI;

//Creating the bones for the object used to store the maps data
function placeObject(placeName, latitude, longitude, latAndLng, riseTime, setTime){
    this.placeName = placeName;
    this.latitude = latitude;
    this.longitude = longitude;
    this.latAndLng = latAndLng;
    this.riseTime = riseTime;
    this.setTime = setTime;
}

//Ajax request skeleton
function ajaxRequest(method, url, async, data, callback){

	var request = new XMLHttpRequest();
	request.open(method,url,async);
	
	if(method == "POST"){
		request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	}
	
	request.onreadystatechange = function(){
		if (request.readyState == 4) {
			if (request.status == 200) {
				var response = request.responseText;
				callback(response);
			} else {
				alert(request.statusText);
			}
		}
    }

	request.send(data);
}

//Initialising the Google maps api and centering on Hamilton
function initialize() {
    map = new google.maps.Map(document.getElementById('mapHolder'), {
      zoom: 8,
      center: {lat: -37.7870012, lng: 175.27925300000004}
    });
    geocoder = new google.maps.Geocoder();
    var search = ""; 
    var poiSearch = "";

    document.getElementById('placeButton').addEventListener('click', function() {
        search = document.getElementById('placeInput').value;

        createObject(search);
        
    });
    
    document.getElementById('poiButton').addEventListener('click', function() {
        poiSearch = document.getElementById('poiInput').value;
        poiFunction(poiSearch);
    });
};

//Create an instance of the object with the appropriate maps data
//Also calls a couple of functions for the sunApi and updating the map
function createObject(search) {

    geocoder.geocode({'address': search}, function(result, status) {
        if (status == 'OK') {
            var placeName = search;
            var latitude = result[0].geometry.location.lat();
            var longitude = result[0].geometry.location.lng();
            var latAndLng = result[0].geometry.location;
            var riseTime;
            var setTime;

            var searchedPlace
            var searchedPlaceString

            searchedPlace = new placeObject(placeName, latitude, longitude, latAndLng, riseTime, setTime);
            placeArray.push(searchedPlace);
           
            searchedPlaceString = '"'+searchedPlace.placeName+'"';

            listDisplay = "<li onclick='Javascript:onClickUpdater("+searchedPlaceString+")'>"+searchedPlace.placeName+"</li>";
            currentPlaceSelection = "<p>"+searchedPlace.placeName+"</p>";

            pastSearches.innerHTML += listDisplay;
            
            sunMovementsFunction(searchedPlace.placeName);
            updateCurrentTitle();
            mapUpdater(latAndLng);
        }
        else {
            alert('No results found, try something else');
        };
    });
};

//Updates map and centers on result
function mapUpdater(coordinates){
    
    for (var i = 0; i < placeArray.length; i++){
        if (coordinates == placeArray[i].latAndLng){

            var latitude1 = placeArray[i].latitude;
            var longitude1 = placeArray[i].longitude;
            usableCoordsForPOI = ""+latitude1+","+longitude1+"" ;
            
        };
    };

    var marker;

    map.setCenter(coordinates);
    map.setZoom(12);
    marker = new google.maps.Marker({
    map: map,
    position: coordinates,
    });
};

//updates a couple of values and calls functions when you click on the stored history list items in ui
function onClickUpdater(searchedPlaceName){

    for (var i = 0; i < placeArray.length; i++){
        if (searchedPlaceName == placeArray[i].placeName){
            
            currentPlaceSelection = "<p>"+placeArray[i].placeName+"</p>";
            mapUpdater(placeArray[i].latAndLng);

            updateCurrentTitle();
            sunMovementsFunction(searchedPlaceName);
        };
    };

}

//Current title is used for the little header about the sunrise/set data as well as keeping track of what is wanted
function updateCurrentTitle(){
    currentSelection.innerHTML = currentPlaceSelection;
}

//Calls sunTracker.php and parses coords for the request
function sunMovementsFunction(searchedName){
    for (var i = 0; i < placeArray.length; i++){
        if (searchedName == placeArray[i].placeName){
            var sunFuncLat = placeArray[i].latitude;
            var sunFuncLng = placeArray[i].longitude;

            ajaxRequest("POST", "sunTracker.php", true, "latitude="+sunFuncLat+"&longitude="+sunFuncLng, sunRequestReturn);
        };
    };
}

//Callback for the ajaxRequest: sunMovementsFunction and updates ui
function sunRequestReturn(riseSet){
    var rise;
    var set;

    results = JSON.parse(riseSet);

    rise = results.results.sunrise; 
    set = results.results.sunset;
    rise = rise.split(" ");
    set = set.split(" ");

    sunriseValue = "<p>Sunrise: "+rise[0]+" AM</p>";
    sunsetValue = "<p>Sunset: "+set[0]+" PM</p>";

    sunRise.innerHTML = ""+sunriseValue+"";
    sunSet.innerHTML = ""+sunsetValue+"";

}

//poi means Point Of Interest not traditional Maori ball on string, this also parses coords to poiSearch.php 
function poiFunction(searchedPOI){

    var data = "searchTerm="+searchedPOI+"&coords="+usableCoordsForPOI;

    ajaxRequest("POST", "poiSearch.php", true, data, poiDisplayFunction);
    
}

//Callback for poiFunction, updates ui by iterating through the results and creating a li element from each
function poiDisplayFunction(resultData){
    var parseResults = JSON.parse(resultData);
    var poiList = document.getElementById("googlePlacesResultsContainer");

    poiList.innerHTML = "<li><u>Points of interest:</u></li>";

    for (var i = 0; i < parseResults.results.length; i++) {
        var locationPOI = JSON.stringify(parseResults.results[i].geometry.location);
        var newListElement = "<li onclick='Javascript:mapUpdaterPOI("+locationPOI+")'>"+parseResults.results[i].name+"</li>";
        googlePlacesResultsContainer.innerHTML += newListElement;
    };
};

//Is called by the onclick functionality of the poi list elements in the ui, basically drops a marker and centers the map
function mapUpdaterPOI(coordinates){
   
    var marker;

    map.setCenter(coordinates);
    map.setZoom(16);
    marker = new google.maps.Marker({
    map: map,
    position: coordinates,
    });
};

